package com.example.flutter_plugin_info_device

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.telephony.TelephonyManager
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList


class DeviceInfoImei {
  fun imeiMain(activity: Activity, applicationContext: Context): String? {
    var imei: String? = ""
    try {
      val telephonyManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        imei = telephonyManager.imei
      }
    } catch (ex: Exception) {
      Timber.d("BuildSerialException getDeviceSerial: $ex")
    }
    return imei;
  }

  fun getImeiMulti(activity: Activity, applicationContext: Context): ArrayList<String> {
    val listImei = ArrayList<String>()
    try {
      val telephonyManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val phoneCount = telephonyManager.phoneCount
        for (i in 0 until phoneCount) {
          listImei.add(telephonyManager.getImei(i))
        }
        return listImei
      }
    } catch (ex: Exception) {
      Timber.d("BuildSerialException getDeviceSerial: $ex")
    }
    return listImei;
  }
}