package com.example.flutter_plugin_info_device

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.PluginRegistry.Registrar

class FlutterPluginInfoDevicePlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  private lateinit var context: Context
  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_plugin_info_device")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
  }

  //  Trate de comprender este registro en el contructor, pero no pude entenderlo
  //  ¿Por que lo hacen a traves de registra.messenger() bajo el tag "fluttertoast"?
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "fluttertoast")
      channel.setMethodCallHandler(FlutterPluginInfoDevicePlugin())
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "getPlatformVersion" -> {
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      "getSerialDevice" -> {
        var infoSerial: String? = ""
        try {
          val properties = DeviceInfoSerial()
          infoSerial = properties.getDeviceSerial(context)
        } catch (e: Error) {
          println("$e");
        }
        result.success("Serial $infoSerial")
      }
      "getUniqueImei" -> {
        var imeis: String? = ""
        try {
          val propertiesImei = DeviceInfoImei()
          imeis = propertiesImei.imeiMain(activity, context)
        } catch (e: Error) {
          println("$e");
        }
        result.success(imeis)
      }
      "getMultiImei" -> {
        var listImei = ArrayList<String>()
        try {
          val propertiesImei = DeviceInfoImei()
          listImei = propertiesImei.getImeiMulti(activity, context)
        } catch (e: Error) {
          println("$e");
        }
        result.success(listImei)
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onDetachedFromActivity() {}
  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    onAttachedToActivity(binding)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity
  }
  override fun onDetachedFromActivityForConfigChanges() {}
}
