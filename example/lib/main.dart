import 'package:flutter/material.dart';
import 'package:flutter_plugin_info_device/flutter_plugin_info_device.dart';
import 'dart:async';

import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _serialDevice = 'Unknown';
  String _uniqueImei = 'Unknown';
  List<String> _multiImei = <String>[];

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await FlutterPluginInfoDevice.platformVersion ??
          'Unknown platform version';
    } catch (e) {
      platformVersion = 'Failed to get platform version.';
    }
    String? serialDevice;
    try {
      final requestPermission = await Permission.phone.request();
      if (requestPermission == PermissionStatus.granted) {
        serialDevice = await FlutterPluginInfoDevice.serialDevice ??
          'Unknown platform version';
      }
    } catch (e) {
      debugPrint(e.toString());
      serialDevice = 'Failed to get serial device.';
    }

    String? uniqueImei;
    try {
      uniqueImei = await FlutterPluginInfoDevice.uniqueImei;
    } catch (e) {
      debugPrint(e.toString());
      uniqueImei = 'Unknown';
    }

    List<String> multiImei = <String>[];
    try {
      final imeisDevie = await FlutterPluginInfoDevice.multiImei;
      multiImei = [...imeisDevie];
    } catch (e) {
      debugPrint(e.toString());
    }

    if (!mounted) return;
    setState(() {
      _platformVersion = platformVersion;
      _serialDevice = serialDevice ?? '';
      _uniqueImei = uniqueImei ?? '';
      _multiImei = multiImei;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Running on: $_platformVersion\n'),
              const SizedBox(height: 20),
              Text('Info device serial: $_serialDevice\n'),
              const SizedBox(height: 20),
              Text('Info device imei: $_uniqueImei\n'),
              const SizedBox(height: 20),
              Text('Info device list Imei: $_multiImei\n'),
            ],
          ),
        ),
      ),
    );
  }
}
