import 'dart:async';

import 'package:flutter/services.dart';

class FlutterPluginInfoDevice {
  static const MethodChannel _channel =
      MethodChannel('flutter_plugin_info_device');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String?> get serialDevice async {
    final String? version = await _channel.invokeMethod('getSerialDevice');
    return version;
  }

  /// This method return an active Imei or the main Imei on Device
  static Future<String?> get uniqueImei async {
    final String? version = await _channel.invokeMethod('getUniqueImei');
    return version;
  }

  /// This method just return a List<String> with all the imeis that device
  /// has associeted
  static Future<List<dynamic>> get multiImei async {
    final List<dynamic> version = await _channel.invokeMethod('getMultiImei');
    return version;
  }
}
